import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {FullComponent} from './layouts/full/full.component';
import {HomeComponent} from './static-page/home/home.component';

export const Approutes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: '/home', pathMatch: 'full'},
      {
        path: 'home',
        loadChildren: () =>
          import('./static-page/static-page.module').then(
            m => m.StaticPageModule,
          ),
      },
      {
        path: 'admin',
        component: FullComponent,
        children: [
          {path: '', redirectTo: '/admin/dashboard', pathMatch: 'full'},
          {
            path: 'dashboard',
            loadChildren: () =>
              import('./dashboard/dashboard.module').then(
                m => m.DashboardModule,
              ),
          },
          {
            path: 'component',
            loadChildren: () =>
              import('./component/component.module').then(
                m => m.ComponentsModule,
              ),
          },
        ],
      },
    ],
  },
  {
    path: '**',
    redirectTo: '/home',
  },
];
