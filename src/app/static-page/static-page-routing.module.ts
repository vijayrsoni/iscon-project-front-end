import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';

export const StaticPagesRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        data: {
          title: 'homePage',
          urls: [
            {title: 'home', url: '/home'},
            {title: 'ngComponent'},
            {title: 'home'},
          ],
        },
      },
    ],
  },
];
